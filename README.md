# 输入网址自动获取favicon图标

#### 项目介绍
使用本接口后，只需要输入目标网站网址就可以获取该网站的ico图标；非常适用于各种导航网站。



#### 效果图

![输入图片说明](https://images.gitee.com/uploads/images/2018/1017/191413_729395c1_2236313.png "3851987266.png")


#### 使用方法

http://您的网址/api/favicon.php?url=目标网址


#### 演示地址

http://demo.hezi.moqingwu.com/favicon/favicon.php?url=https://www.baidu.com



#### 开发者

孟坤博客


#### 更多源码

http://hezi.moqingwu.com
